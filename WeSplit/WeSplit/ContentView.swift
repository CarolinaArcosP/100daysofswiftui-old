//
//  ContentView.swift
//  WeSplit
//
//  Created by Carolina Arcos on 3/29/21.
//

import SwiftUI

struct ContentView: View {
    @State private var checkAmount: String = ""
    @State private var numberOfPeople: Int = 0
    @State private var tipPercentage: Int = 0
    
    private let tipPercentages = [0, 5, 10, 15, 20, 25]
    
    var amount: Double {
        return Double(checkAmount) ?? 0
    }
    var tipAmount: Double {
        let tip = Double(tipPercentages[tipPercentage])
        return amount * tip
    }
    
    var total: Double {
        return amount + tipAmount
    }
    
    var totalPerPerson: Double {
        let peopleCount = Double(numberOfPeople + 2)
        let totalAmount = amount + tipAmount
        return totalAmount / peopleCount
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    TextField("Check total amount", text: $checkAmount)
                        .keyboardType(.decimalPad)
                    Picker("Number of people", selection: $numberOfPeople, content: {
                        ForEach(2 ..< 99) {
                            Text("\($0) people")
                        }
                    })
                }
                
                Section(header: Text("How much tip do you want to leave?")) {
                    Picker("Tip percentage", selection: $tipPercentage) {
                        ForEach(0 ..< tipPercentages.count) {
                            Text("\(self.tipPercentages[$0])%")
                        }
                    }
                    .pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("Total resume")) {
                    Text("Check: \(checkAmount)")
                    Text("Tip: \(tipAmount, specifier: "%.2f")")
                        .foregroundColor(tipPercentage == 0 ? .red : .black)
                    Text("TOTAL: \(total, specifier: "%.2f")")
                }
                
                Section(header: Text("Total per person")) {
                    Text("\(totalPerPerson, specifier: "%.2f")")
                }
            }
            .navigationBarTitle("WeSplit")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
