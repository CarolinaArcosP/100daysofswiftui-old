//
//  UnitConversionsView.swift
//  WeSplit
//
//  Created by Carolina Arcos on 3/30/21.
//

import SwiftUI

struct UnitConversionsView: View {
    @State private var baseValue: String = ""
    @State private var baseUnit: TempUnit = TempUnit.celsius
    @State private var targetUnit: TempUnit = TempUnit.celsius
        
    private var initValue: Double? {
        return Double(baseValue)
    }
    
    private var initialCelsiusValue: Double? {
        guard let initialDoubleValue = initValue else { return nil }
        switch baseUnit {
        case .celsius:
            return initialDoubleValue
        case .fahrenheit:
            return (initialDoubleValue - 32) * (5/9)
        case .kelvin:
            return initialDoubleValue - 273.15
        }
    }
    
    private var targetValue: Double? {
        guard let initialDoubleValue = initialCelsiusValue else { return nil }
        switch targetUnit {
        case .celsius:
            return initialDoubleValue
        case .fahrenheit:
            return (initialDoubleValue * (9/5)) + 32
        case .kelvin:
            return initialDoubleValue + 273.15
        }
    }
    
    private var formatedTargetValue: String {
        guard let targetValue = targetValue else {
            return ""
        }
        return String(format: "%.2f", targetValue)
    }
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Base unit")) {
                    TextField("Base unit value", text: $baseValue)
                        .keyboardType(.decimalPad)
                    Picker("Base unit", selection: $baseUnit) {
                        ForEach(TempUnit.allCases, id: \.self) {
                            Text($0.rawValue)
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                }
                
                Section(header: Text("Target unit")) {
                    Text("\(formatedTargetValue)")
                    Picker("Base unit", selection: $targetUnit) {
                        ForEach(TempUnit.allCases, id: \.self) {
                            Text($0.rawValue)
                        }
                    }.pickerStyle(SegmentedPickerStyle())
                }
            }
            .navigationBarTitle("Unit conversion")
        }
    }
}

struct UnitConversionsView_Previews: PreviewProvider {
    static var previews: some View {
        UnitConversionsView()
    }
}

private enum TempUnit: String, CaseIterable {
    case celsius = "Celsius"
    case fahrenheit = "Fahrenheit"
    case kelvin = "Kelvin"
}
