//
//  CustomViews.swift
//  RPSQuest
//
//  Created by Carolina Arcos on 4/14/21.
//

import SwiftUI

struct Title: View {
    let title: String
    
    var body: some View {
        Text(title)
            .bold()
            .kerning(1.5)
            .font(.title)
            .foregroundColor(.black)
            .padding(EdgeInsets(top: 20, leading: 10, bottom: 20, trailing: 10))
            .frame(maxWidth: .infinity, alignment: .center)
    }
}

struct IndicatorText: View {
    let indicator: String
    let value: String
    
    var body: some View {
        HStack(alignment: .center, spacing: 8, content: {
            Text(indicator).indicator()
            Text(value.uppercased()).indicatorValue()
        })
    }
}

struct GameElement: View {
    let name: String
    
    var body: some View {
        ZStack(alignment: Alignment(horizontal: .center, vertical: .center)) {
            Color.white
            ElementImage(name: name)
        }
        .frame(width: 80, height: 80, alignment: .center)
        .clipShape(Circle())
        .overlay(Circle().stroke(Color.black, lineWidth: 1.0))
    }
}

struct ElementImage: View {
    let name: String
    
    var body: some View {
        Image(name)
            .resizable()
            .frame(width: 60, height: 60, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)

    }
}
