//
//  ContentView.swift
//  RPSQuest
//
//  Created by Carolina Arcos on 4/13/21.
//

import SwiftUI

struct ContentView: View {
    @State private var alertIsShown = false
    @State private var resultIsShown = false
    @State private var isCorrect = false
    @State private var shouldWin = Bool.random()
    @State private var appSelection = Option.allCases.randomElement() ?? .rock
    @State private var score = 0
    @State private var round = 1
    
    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [Color.blue, Color.purple]),
                           startPoint: .top,
                           endPoint: .bottom).edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            VStack(alignment: .center, spacing: 35, content: {
                Title(title: "Rock Paper Scissors Quest")
                ElementImage(name: appSelection.rawValue)
                IndicatorText(indicator: "Play for",
                              value: shouldWin ? "win" : "lose")
                HStack(alignment: .center, spacing: 20) {
                    ForEach (Option.allCases, id: \.self) { option in
                        Button(action: {
                            optionSelected(option)
                        }, label: {
                            GameElement(name: option.rawValue)
                        })
                    }
                }
                Spacer()
                HStack(alignment: .center, spacing: 50, content: {
                    IndicatorText(indicator: "Score:", value: "\(score)")
                    IndicatorText(indicator: "Round:", value: "\(round)")
                })
                Spacer()
            })
            .alert(isPresented: $alertIsShown, content: {
                Alert(title: Text("End of game"),
                      message: Text("Your score is \(score)"),
                      dismissButton: .default(Text("Play again")) {
                        resetGame()
                      })
            })
            if resultIsShown {
                Image(isCorrect ? "check" : "error")
            }
        }
    }
    
    func optionSelected(_ selection: Option) {
        let expectedOption = appSelection.optionFor(win: shouldWin)
        isCorrect = expectedOption == selection
        score += isCorrect ? 1 : -1
        
        if round == 10 {
            alertIsShown = true
        } else {
            round += 1
            showResult()
            setNewRound()
        }
    }
    
    func setNewRound() {
        shouldWin = Bool.random()
        appSelection = Option.allCases.randomElement() ?? .rock
    }
    
    func resetGame() {
        score = 0
        round = 1
        setNewRound()
    }
    
    func showResult() {
        resultIsShown = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            resultIsShown = false
        }
    }
}

enum Option: String, CaseIterable {
    case rock = "Rock"
    case paper = "Paper"
    case scissors = "Scissors"
    
    func optionFor(win: Bool) -> Self {
        switch (self, win) {
        case (.rock, true): return .paper
        case (.rock, false): return .scissors
        case (.paper, true): return .scissors
        case (.paper, false): return .rock
        case (.scissors, true): return .rock
        case (.scissors, false): return .paper
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
