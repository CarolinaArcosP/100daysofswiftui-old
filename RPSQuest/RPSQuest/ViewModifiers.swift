//
//  ViewModifiers.swift
//  RPSQuest
//
//  Created by Carolina Arcos on 4/13/21.
//

import SwiftUI

struct Indicator: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.title2)
            .foregroundColor(.black)
    }

}

struct IndicatorValue: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.title3)
            .foregroundColor(.black)
            .shadow(color: .black, radius: 0.5, x: 0.0, y: 0.0)
    }
}

extension View {
    func indicator() -> some View {
        self.modifier(Indicator())
    }
    
    func indicatorValue() -> some View {
        self.modifier(IndicatorValue())
    }
}
