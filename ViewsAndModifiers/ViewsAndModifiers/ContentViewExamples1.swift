//
//  ContentViewExamples1.swift
//  ViewsAndModifiers
//
//  Created by Carolina Arcos on 4/6/21.
//

import SwiftUI

extension View {
    func circleButtonStyle() -> some View {
        self.modifier(CircleButton())
    }
    
    func waterMarked() -> some View {
        self.modifier(Watermarked(text: "100 days\n of Swift UI"))
    }
    
    func prominentTitle() -> some View {
        self.modifier(ProminentTitle())
    }
}

struct CircleButton: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(width: 80, height: 80, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .font(.largeTitle)
            .padding()
            .background(Color.black)
            .clipShape(Circle())
    }
}

struct Watermarked: ViewModifier {
    let text: String
    
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing, content: {
            content
            Text(text)
                .font(.caption)
                .foregroundColor(.white)
                .padding(5)
                .background(Color.black)
            
        })
    }
}

struct ProminentTitle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
            .font(.largeTitle)
    }
}

struct CapsuleButton: View {
    let title: String
    
    var body: some View {
        Button(title) {
            print("Capsule button pressed")
        }
        .font(.subheadline)
        .foregroundColor(.white)
        .padding()
        .background(Color.blue)
        .clipShape(Capsule())
    }
}

struct ContentViewExamples1: View {
    private let againButton = CapsuleButton(title: "Wave, again")
    
    @State private var useRed = false

    private var helloButton: some View {
        Button("H, W!") {
            useRed.toggle()
        }
    }
    private var waveStack: some View {
        VStack {
            Spacer()
            helloButton
                .circleButtonStyle()
            againButton
                .font(.body)
            CapsuleButton(title: "Just print")
            Text("Large text")
                .prominentTitle()
            Spacer()
        }
    }
    
    var body: some View {
        waveStack
            .font(.title)
            .padding()
            .background(Color.yellow)
            .padding()
            .background(useRed ? Color.red : Color.green)
            .waterMarked()
    }
}

struct ContentViewExamples1_Previews: PreviewProvider {
    static var previews: some View {
        ContentViewExamples1()
    }
}
