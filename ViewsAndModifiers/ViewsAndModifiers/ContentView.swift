//
//  ContentView.swift
//  ViewsAndModifiers
//
//  Created by Carolina Arcos on 4/11/21.
//

import SwiftUI

struct GridStack<Content: View> : View {
    let rows: Int
    let columns: Int
    let content: (Int, Int) -> Content
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            ForEach (0 ..< rows) { row in
                HStack(alignment: .center, spacing: 5) {
                    ForEach ( 0 ..< columns) { column in
                        self.content(row, column)
                    }
                }
            }
        }
    }
}

struct ContentView: View {
    var body: some View {
        GridStack(rows: 4, columns: 4) { row, column in
            HStack(alignment: .center, spacing: 0) {
                Image(systemName: "\(row * 4 + column).circle")
                Text("R\(row)-C\(column)")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
