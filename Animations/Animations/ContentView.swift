//
//  ContentView.swift
//  Animations
//
//  Created by Carolina Arcos on 5/5/21.
//

import SwiftUI

struct CornerRotateModifier: ViewModifier {
    let amount: Double
    let anchor: UnitPoint
    
    func body(content: Content) -> some View {
        content
            .rotationEffect(.degrees(amount), anchor: anchor)
            .clipped()
    }
}

extension AnyTransition {
    static var pivot: AnyTransition {
        .modifier(active: CornerRotateModifier(amount: -90, anchor: .topLeading),
                  identity: CornerRotateModifier(amount: 0, anchor: .topLeading))
    }
}

struct ContentView: View {
    private let letters = Array("Hello SwiftUI!")
    @State private var dragAmount = CGSize.zero
    @State private var enabled = false
    @State private var lettersDragAmount = CGSize.zero
    @State private var showingRed = false
    
    var body: some View {
        VStack {
            LinearGradient(gradient: Gradient(colors: [Color.yellow, Color.red]), startPoint: .topLeading, endPoint: .topTrailing)
                .frame(width: 200, height: 200)
                .clipShape(RoundedRectangle(cornerRadius: 25.0))
                .offset(dragAmount)
                .gesture(
                    DragGesture()
                        .onChanged { dragAmount = $0.translation }
                        .onEnded({ _ in
                            withAnimation(.spring()) {
                                dragAmount = .zero
                            }
                        })
                )
            
            Spacer()
            
            HStack(spacing: 2) {
                ForEach (0..<letters.count) { pos in
                    Text(String(letters[pos]))
                        .padding(5)
                        .font(.title)
                        .background(enabled ? Color.green : Color.blue)
                        .offset(lettersDragAmount)
                        .animation(Animation.default.delay(Double(pos) / 20))
                }
            }
            .gesture(
                DragGesture()
                    .onChanged { lettersDragAmount = $0.translation }
                    .onEnded { _ in
                        enabled.toggle()
                        lettersDragAmount = .zero
                    }
            )
            
            Spacer()
            
            VStack {
                Button("Tap me") {
                    withAnimation {
                        showingRed.toggle()
                    }
                }
                
                HStack {
                    if showingRed {
                        Rectangle()
                            .fill(Color.pink)
                            .frame(width: 100, height: 100)
                            .transition(.asymmetric(insertion: .scale, removal: .opacity))
                        
                        Rectangle()
                            .fill(Color.purple)
                            .frame(width: 100, height: 100)
                            .transition(.pivot)
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
