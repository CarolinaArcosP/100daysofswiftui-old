//
//  AnimationsContentView.swift
//  Animations
//
//  Created by Carolina Arcos on 5/4/21.
//

import SwiftUI

struct AnimationsContentView: View {
    @State private var animationAmount: CGFloat = 1
    @State private var animationButtonAmount: CGFloat = 1
    @State private var animationAmount3d = 0.0
    @State private var enabled = false
    
    var body: some View {
        VStack {
            Stepper("Scale amount", value: $animationAmount.animation(
                Animation.easeInOut(duration: 1)
                .repeatCount(3, autoreverses: true)
            ), in: 1...10) // Binding animation
            
            Spacer()
            
            Button("Tap me") {
                animationAmount += 1
            }
            .padding(30)
            .background(Color.red)
            .foregroundColor(.white)
            .clipShape(Circle())
            .overlay(Circle()
                        .stroke()
                        .scaleEffect(animationButtonAmount)
                        .opacity(Double(2 - animationButtonAmount))
                        .animation(
                            Animation
                                .easeInOut(duration: 1)
                                .repeatForever(autoreverses: false)
                        )) // Implicit animation (attached to a view)
            .onAppear(perform: {
                animationButtonAmount = 2
            })
            .scaleEffect(animationAmount)
            
            Spacer()
            
            Button("Tap me") {
                withAnimation(.interpolatingSpring(stiffness: 5, damping: 1)) {
                    animationAmount3d += 360
                }
            }
            .padding(30)
            .background(Color.green)
            .foregroundColor(.white)
            .clipShape(Circle())
            .rotation3DEffect(
                .degrees(animationAmount3d),
                axis: (x: 0.0, y: 1.0, z: 0.0)) // explicit animation
            
            Spacer()
            
            Button("Tap me") {
                enabled.toggle()
            }
            .frame(width: 200, height: 200, alignment: .center)
            .foregroundColor(.white)
            .background(enabled ? Color.blue : Color.red)
            .animation(enabled ? nil : .default)
            .clipShape(RoundedRectangle(cornerRadius: enabled ? 60 : 0))
            .animation(.interpolatingSpring(stiffness: 20, damping: 1))
        }
    }
}

struct AnimationsContentView_Previews: PreviewProvider {
    static var previews: some View {
        AnimationsContentView()
    }
}
