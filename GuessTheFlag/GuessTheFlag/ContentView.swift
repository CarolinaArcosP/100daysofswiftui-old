//
//  ContentView.swift
//  GuessTheFlag
//
//  Created by Carolina Arcos on 3/31/21.
//

import SwiftUI

struct FlagImage: View {
    let imageName: String
    
    var body: some View {
        Image(imageName)
            .renderingMode(.original)
            .clipShape(Capsule())
            .overlay(Capsule().stroke(Color.black, lineWidth: 1.0))
            .shadow(color: .black, radius: 2, x: 1.0, y: 3.0)
    }
}

struct ContentView: View {
    @State private var showingScore = false
    @State private var optionSelected = false
    @State private var chooseOK = false
    @State private var scoreTitle = ""
    @State private var scoreMessage = ""
    @State private var score = 0
    @State private var successAnimationAmount = 0.0
    @State private var userSelection = -1
    @State private var correctCountyId = Int.random(in: 0 ... 2)
    @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Russia", "Spain", "UK", "USA", "Monaco"].shuffled()
    
    var body: some View {
        VStack(alignment: .trailing, spacing: 20) {
            ZStack(alignment: .center) {
                AngularGradient(gradient: Gradient(colors: [.blue, .green, .blue]), center: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/).edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                
                VStack(spacing: 30) {
                    Spacer()
                    
                    VStack {
                        Text("Tap the flag of")
                            .font(.title3)
                            .fontWeight(.light)
                            .tracking(2)
                        Text(countries[correctCountyId])
                            .font(.largeTitle)
                            .fontWeight(.black)
                            .tracking(2)
                    }
                    
                    ForEach(0 ..< 3) { number in
                        Button(action: {
                            flagTapped(number)
                        }, label: {
                            FlagImage(imageName: countries[number])
                                .opacity(shouldAddOpacity(number) ? 0.25 : 1)
                                .offset(x: shouldOffset(number) ? -10 : 0, y: 0)
                                .rotation3DEffect(
                                    .degrees(shouldRotate(number) ? successAnimationAmount : 0),
                                    axis: (x: 0.0, y: 1.0, z: 0.0))
                                .animation(animation(number))
                        })
                    }
                    
                    HStack(alignment: .center, spacing: 30, content: {
                        Text("Current Score:")
                            .font(.title3)
                            .fontWeight(.medium)
                            .tracking(2)
                        Text("\(score) pts")
                            .font(.title2)
                            .fontWeight(.medium)
                            .tracking(2)
                    })
                    
                    Button(action: {
                        score = 0
                    }, label: {
                        Text("Restart")
                            .foregroundColor(.white)
                            .font(.title3)
                            .fontWeight(.medium)
                            .tracking(2)
                            .frame(width: 200, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .background(Color.blue)
                            .clipShape(Capsule())
                            .overlay(Capsule().stroke(Color.blue, lineWidth: /*@START_MENU_TOKEN@*/1.0/*@END_MENU_TOKEN@*/))
                    })
                    
                    Spacer()
                }
            }
            .alert(isPresented: $showingScore, content: {
                Alert(title: Text(scoreTitle),
                      message: Text(scoreMessage),
                      dismissButton: .default(Text("Continue")) {
                        askQuestion()
                      })
            })
        }
    }
    
    private func flagTapped(_ selectedId: Int) {
        userSelection = selectedId
        chooseOK = userSelection == correctCountyId
//        showingScore = true
        score += chooseOK ? 5 : 0
        setMessage()
        showResult()
    }
    
    private func showResult() {
        optionSelected = true
        successAnimationAmount += chooseOK ? 360 : 0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            optionSelected = false
            askQuestion()
        }
    }
    
    private func setMessage() {
        if chooseOK {
            scoreTitle = "Correct"
            scoreMessage = "You earned 5 points"
        } else {
            scoreTitle = "Wrong"
            scoreMessage = "That's the flag of \(countries[userSelection])"
        }
    }
    
    private func askQuestion() {
        countries.shuffle()
        correctCountyId = Int.random(in: 0 ... 2)
    }
    
    // MARK: - Animation validations
    // Opacity is added to the flags that the user did not select, no matter if is correct or wrong
    func shouldAddOpacity(_ number: Int) -> Bool {
        guard optionSelected else { return false }
        if chooseOK {
            return number != correctCountyId
        } else {
            return number != userSelection
        }
    }
    
    // Offset is applied to the flag the user selects when the answer is wrong
    func shouldOffset(_ number: Int) -> Bool {
        return !chooseOK && number == userSelection && optionSelected
    }
    
    // Rotation is applied to the flag the user selects when the answer is correct
    func shouldRotate(_ number: Int) -> Bool {
        return chooseOK && number == correctCountyId && optionSelected
    }
    
    func animation(_ number: Int) -> Animation? {
        guard optionSelected else { return nil }
        return shouldOffset(number) ? Animation.default.repeatCount(2).speed(5) : Animation.default
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
