//
//  GameView.swift
//  EdutainmentApp
//
//  Created by Carolina Arcos on 6/2/21.
//

import SwiftUI

struct GameView: View {
    @State private var userAnswer = ""
    @State private var currentQuestion = 0
    @State private var alertTitle = ""
    @State private var alertMessage = ""
    @State private var isShowingAlert = false
    @State private var score = 0
    @State private var finishGame = false
    
    let multiplicationTable: Int
    let questions: [Int]
    let restartCompletion: () -> Void
    
    var body: some View {
        VStack(alignment: .center, spacing: 15, content: {
            Spacer()
            if !finishGame {
                Text("Answer...")
                    .bold()
                    .font(.title)
                Text("\(multiplicationTable) x \(questions[currentQuestion]) =")
                    .font(.title3)
                TextField("Your answer", text: $userAnswer)
                    .keyboardType(.numberPad)
                Button("Submit", action: {
                    submitAnswer()
                })
            } else {
                Text("Your Score is")
                    .bold()
                    .font(.title)
                Text("\(score) / \(questions.count)")
                    .font(.title3)
                Button("Finish", action: {
                    restartCompletion()
                })
            }
            Spacer()
        })
        .alert(isPresented: $isShowingAlert, content: {
            Alert(title: Text(alertTitle),
                  message: Text(alertMessage), dismissButton: .default(Text("Continue"), action: {
                    nextQuestion()
                  }))
        })
    }
    
    func submitAnswer() {
        let answer = multiplicationTable * questions[currentQuestion]
        if answer == Int(userAnswer) ?? 0 {
            score += 1
            alertTitle = "Correct!"
            alertMessage = "Good job!"
        } else {
            alertTitle = "Wrong!"
            alertMessage = "The correct answer is \(answer)"
        }
        
        isShowingAlert = true
    }
    
    func nextQuestion() {
        isShowingAlert = false
        currentQuestion += 1
        finishGame = currentQuestion == questions.count
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(multiplicationTable: 4, questions: [4,2,8,9], restartCompletion: { })
    }
}
