//
//  EdutainmentAppApp.swift
//  EdutainmentApp
//
//  Created by Carolina Arcos on 5/26/21.
//

import SwiftUI

@main
struct EdutainmentAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
