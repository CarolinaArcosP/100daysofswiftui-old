//
//  ContentView.swift
//  EdutainmentApp
//
//  Created by Carolina Arcos on 5/26/21.
//

import SwiftUI

struct ContentView: View {
    @State private var isShowingSettings = true
    @State private var selectedMultiplicationTable = 2
    @State private var selectedNumberOfQuestions = 2
    
    var body: some View {
        Group {
            if isShowingSettings {
                VStack {
                    Form {
                        VStack(alignment: .leading, spacing: 15, content: {
                            Text("Multiplication table to practice")
                                .bold()
                            Stepper(value: $selectedMultiplicationTable, in: 2...12, step: 1) {
                                Text("Table of \(selectedMultiplicationTable)")
                            }
                        })
                        VStack(alignment: .leading, spacing: 15, content: {
                            Text("How many questions?")
                                .bold()
                            Stepper(value: $selectedNumberOfQuestions, in: 2...10, step: 1) {
                                Text("\(selectedNumberOfQuestions) questions")
                            }
                        })
                    }
                    Button("Start!", action: {
                        setGameState()
                    })
                    Spacer()
                }
            } else {
                GameView(multiplicationTable: selectedMultiplicationTable,
                         questions: generateRandomQuestions(),
                         restartCompletion: { setConfigurationState() })
            }
        }
    }
    
    func setConfigurationState() {
        isShowingSettings = true
        selectedMultiplicationTable = 2
        selectedNumberOfQuestions = 2
    }
    
    func setGameState() {
        isShowingSettings = false
    }
    
    func generateRandomQuestions() -> [Int] {
        var questions = Set<Int>()
        for _ in 1...selectedNumberOfQuestions {
            var random = Int.random(in: 1...10)
            var inserted = questions.insert(random).inserted
            while !inserted {
                random = Int.random(in: 1...10)
                inserted = questions.insert(random).inserted
            }
        }
        
        return Array(questions)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
