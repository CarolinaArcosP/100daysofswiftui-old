//
//  ExpenseItem.swift
//  iExpense
//
//  Created by Carolina Arcos on 6/2/21.
//

import Foundation

struct ExpenseItem: Identifiable, Codable { // This type can be identified uniquely
    var id = UUID() // Generates a unique UUDI automatically
    let name: String
    let type: String
    let amount: Int
}

class Expenses: ObservableObject {
    @Published var items = [ExpenseItem]() {
        didSet {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(items) {
                UserDefaults.standard.set(encoded, forKey: "Items")
            }
        }
    }
    
    init() {
        if let itemsStored = UserDefaults.standard.data(forKey: "Items") {
            let decoder = JSONDecoder()
            if let decoded = try? decoder.decode([ExpenseItem].self, from: itemsStored) {
                items = decoded
                return
            }
        }
        
        items = []
    }
}
