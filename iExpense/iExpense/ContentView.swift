//
//  ContentView.swift
//  iExpense
//
//  Created by Carolina Arcos on 6/2/21.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject private var expenses = Expenses()
    @State private var showingAddExpense = false
    
    var body: some View {
        NavigationView {
            List {
                ForEach(expenses.items) { item in
                    HStack {
                        VStack(alignment: .leading, spacing: 5) {
                            Text(item.name)
                                .font(.headline)
                            Text(item.type)
                        }
                        Spacer()
                        Text("\(item.amount)")
                            .foregroundColor(getAmountColor(item.amount))
                    }
                }
                .onDelete(perform: removeItems)
            }
            .navigationBarTitle("iExpense")
            .navigationBarItems(trailing:
                                    HStack(alignment: .center, spacing: 20) {
                                        EditButton()
                                        Button(action: {
                                            showingAddExpense = true
                                        }, label: {
                                            Image(systemName: "plus")
                                        })
                                    }
            )
            .sheet(isPresented: $showingAddExpense) {
                AddView(expenses: expenses)
            }
        }
    }
    
    func removeItems(at offsets: IndexSet) {
        expenses.items.remove(atOffsets: offsets)
    }
    
    func getAmountColor(_ amount: Int) -> Color {
        switch amount {
        case 0...10:
            return .black
        case 10...100:
            return .orange
        default:
            return .red
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
