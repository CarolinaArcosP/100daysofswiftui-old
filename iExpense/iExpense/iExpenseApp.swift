//
//  iExpenseApp.swift
//  iExpense
//
//  Created by Carolina Arcos on 6/2/21.
//

import SwiftUI

@main
struct iExpenseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
