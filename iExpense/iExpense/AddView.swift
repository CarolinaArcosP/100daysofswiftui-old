//
//  AddView.swift
//  iExpense
//
//  Created by Carolina Arcos on 6/2/21.
//

import SwiftUI

struct AddView: View {
    @Environment(\.presentationMode) var presentationMode

    @ObservedObject var expenses: Expenses
    @State private var name = ""
    @State private var type = ""
    @State private var amount = ""
    @State private var showingAlert = false
    @State private var alertTitle = "Invalid data"
    @State private var alertMessage = ""
    
    private let types = ["Business", "Personal"]
    
    var body: some View {
        NavigationView {
            Form {
                TextField("Name", text: $name)
                Picker("Type", selection: $type) {
                    ForEach(types, id: \.self) {
                        Text($0)
                    }
                }.pickerStyle(SegmentedPickerStyle())
                TextField("Amount", text: $amount)
            }
            .navigationTitle("Add new expense")
            .navigationBarItems(trailing: Button("Save", action: {
                saveExpense()
            }))
            .alert(isPresented: $showingAlert) {
                Alert(title: Text(alertTitle),
                      message: Text(alertMessage),
                      dismissButton: .default(Text("OK")))
            }
        }
        
    }
    
    func saveExpense() {
        guard !name.isEmpty && !type.isEmpty else {
            setInvalidDataMessage()
            return
        }
        
        
        guard let actualAmount = Int(amount) else {
            setInvalidAmountMessage()
            return
        }
        
        let item = ExpenseItem(name: name, type: type, amount: actualAmount)
        expenses.items.append(item)
        presentationMode.wrappedValue.dismiss()
    }
    
    func setInvalidAmountMessage() {
        alertMessage = "The amount needs to be an Int value"
        showingAlert = true
    }
    
    func setInvalidDataMessage() {
        alertMessage = "All fields are required"
        showingAlert = true
    }
}

struct AddView_Previews: PreviewProvider {
    static var previews: some View {
        AddView(expenses: Expenses())
    }
}
