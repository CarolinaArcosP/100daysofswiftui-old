//
//  ObservableContentView.swift
//  iExpense
//
//  Created by Carolina Arcos on 6/2/21.
//

import SwiftUI

class User: ObservableObject {
    @Published var firstName = "Caro" //tells swift that when the value chanes, notify that
    var lastName = "Arcos"
}

struct ObservableContentView: View {
    @ObservedObject private var user = User() //observedObject to know when the properties had changed.
    
    var body: some View {
        Text("Your name is: \(user.firstName)")
        TextField("First name", text: $user.firstName)
    }
}

struct ObservableContentView_Previews: PreviewProvider {
    static var previews: some View {
        ObservableContentView()
    }
}
