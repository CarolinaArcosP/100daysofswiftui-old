//
//  CodableContentView.swift
//  iExpense
//
//  Created by Carolina Arcos on 6/2/21.
//

import SwiftUI

struct Profile: Codable {
    var firstName: String
    var lastName: String
    
    
}

struct CodableContentView: View {
    @State private var user = Profile(firstName: "Caro", lastName: "Arcos")
    var body: some View {
        Button("Save user") {
            let encoder = JSONEncoder()
            if let data = try? encoder.encode(user) {
                UserDefaults.standard.set(data, forKey: "UserData")
            }
        }
    }
}

struct CodableContentView_Previews: PreviewProvider {
    static var previews: some View {
        CodableContentView()
    }
}
