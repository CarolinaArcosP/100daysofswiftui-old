//
//  UserDefaultsContentView.swift
//  iExpense
//
//  Created by Carolina Arcos on 6/2/21.
//

import SwiftUI

// UserDefaults no more than 512kiloBytes

struct UserDefaultsContentView: View {
    @State private var tapCount = UserDefaults.standard.integer(forKey: "Tap") // 0 by defaults
    var body: some View {
        Button("Tap count: \(tapCount)") {
            tapCount += 1
            UserDefaults.standard.set(tapCount, forKey: "Tap")
        }
    }
}

struct UserDefaultsContentView_Previews: PreviewProvider {
    static var previews: some View {
        UserDefaultsContentView()
    }
}
