//
//  OnDeleteContentView.swift
//  iExpense
//
//  Created by Carolina Arcos on 6/2/21.
//

import SwiftUI

struct OnDeleteContentView: View {
    @State private var numbers = [Int]()
    @State private var currentNumber = 1
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    ForEach(numbers, id: \.self) { // onDelete only exists on ForEach
                        Text("\($0)")
                    }
                    .onDelete(perform: removeRows)
                }
                
                Button("Add number") {
                    numbers.append(currentNumber)
                    currentNumber += 1
                }
            }
            .navigationBarItems(leading: EditButton())
        }
        
    }
    
    func removeRows(at offsets: IndexSet) {
        numbers.remove(atOffsets: offsets)
    }
}

struct OnDeleteContentView_Previews: PreviewProvider {
    static var previews: some View {
        OnDeleteContentView()
    }
}
