//
//  PresentationContentView.swift
//  iExpense
//
//  Created by Carolina Arcos on 6/2/21.
//

import SwiftUI

struct SecondView: View {
    @Environment(\.presentationMode) var presentationMode
    var name: String
    
    var body: some View {
        VStack {
            Text("Hello \(name)")
            Button("Dismiss") {
                presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

struct PresentationContentView: View {
    @State private var showingSheet = false
    
    var body: some View {
        Button("Say hello") {
            showingSheet.toggle()
        }
        .sheet(isPresented: $showingSheet) {
            SecondView(name: "Caro")
        }
    }
}

struct PresentationContentView_Previews: PreviewProvider {
    static var previews: some View {
        PresentationContentView()
    }
}
