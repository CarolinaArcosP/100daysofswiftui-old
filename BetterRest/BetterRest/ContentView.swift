//
//  ContentView.swift
//  BetterRest
//
//  Created by Carolina Arcos on 4/21/21.
//

import SwiftUI

struct ContentView: View {
    @State private var sleepHours = 8.0
    @State private var wakeUp = defaultDateTime
    @State private var coffeeAmount = 1
    
    var recommendedBedtime: String {
        return calculatedBedTime() ?? "Error in calculation"
    }
    
    var body: some View {
        NavigationView {
            Form {
                VStack(alignment: .leading, spacing: 5) {
                    Text("When do you want to wake up?")
                        .font(.headline)
                    DatePicker("Please enter a time", selection: $wakeUp, displayedComponents: .hourAndMinute)
                        .labelsHidden()
                        .datePickerStyle(WheelDatePickerStyle())
                }
                
                VStack(alignment: .leading, spacing: 5) {
                    Text("Desired amount to sleep")
                        .font(.headline)
                    Stepper(value: $sleepHours, in: 4...12, step: 0.25) {
                        Text("\(sleepHours, specifier: "%g") hours")
                    }
                }
                
                VStack(alignment: .leading, spacing: 5) {
                    Picker("Daily coffee intake", selection: $coffeeAmount) {
                        ForEach (1..<21) { number in
                            if number == 1 {
                                Text("1 cup")
                            } else {
                                Text("\(number) cups")
                            }
                        }
                    }
                }
                
                Section(header: Text("Recommended bed time")) {
                    Text("\(recommendedBedtime)")
                        .font(.largeTitle)
                }
            }
            .navigationBarTitle("BetterRest")
        }
    }
    
    static var defaultDateTime: Date {
        var components = DateComponents()
        components.hour = 8
        components.minute = 0
        return Calendar.current.date(from: components) ?? Date()
    }
    
    func calculatedBedTime() -> String? {
        guard let model = try? SleepCalculator(contentsOf: SleepCalculator.urlOfModelInThisBundle) else { return nil }
        
        let components = Calendar.current.dateComponents([.hour, .minute], from: wakeUp)
        let hour = (components.hour ?? 0) * 60 * 60
        let minute = (components.minute ?? 0) * 60
        
        do {
            let prediction = try model.prediction(
                wake: Double(hour + minute),
                estimatedSleep: sleepHours,
                coffee: Double(coffeeAmount))
            let sleepTime = wakeUp - prediction.actualSleep
            let format = DateFormatter()
            format.timeStyle = .short
            
            return format.string(from: sleepTime)
        } catch {
            return nil
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
